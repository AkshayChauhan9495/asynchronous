/*Problem 1: Write a function that will return a particular board's information based on the boardID that 
is passed from the given list of boards in boards.json and then pass control back to the code that called
it by using a callback function.*/


const boards = require("./input/boards.json");

function callback1(boardID, boards) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let result = boards.find(element => element.id === boardID);
            if (result) {
                resolve(result);
            } else {
                reject(new Error("Invalid Board Id"));
            }
        }, 2000)
    })
}

module.exports = callback1;