/* 
    Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

const lists = require("./input/lists.json");
function callback2(boardID, lists) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {

            let result = lists[boardID];
            if (result) {
                resolve(result)
            }
            else {
                reject(new Error("Invalid Boards Id"))
            }


        }, 2000)
    })
}

module.exports = callback2;