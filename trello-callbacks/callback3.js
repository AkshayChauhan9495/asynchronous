/* 
    Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

var cards = require("./input/cards.json");
function callback3(listID, cards) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let result = cards[listID];
            if (result) {
                resolve(result);
            }
            else {
                reject(new Error("Invalid Board ID"));
            }
        }, 2000)
    })
}

module.exports = callback3;