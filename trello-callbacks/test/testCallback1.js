const callback1 = require("../callback1.js");
const boards = require("../input/boards.json");

callback1("mcu453ed", boards)
    .then(function (data) {
        console.log(data)
    }).catch(function (error) {
        console.log(error);
    });